import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EMPTY, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { API_URL } from 'src/environments/environment';
import { Plataforma } from './plataforma.model';

@Injectable({
  providedIn: 'root'
})
export class PlataformaService {

  constructor(
    private snackBar: MatSnackBar,
    private http: HttpClient
  ) { }

  showMessage(msg: string, isError: boolean = false): void {
    this.snackBar.open(msg, 'x', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: isError ? ['msg-error'] : ['msg-success'],
    })
    console.log(msg)
  }

  create(plataforma: Plataforma): Observable<Plataforma> {
    const url = `${API_URL}/plataformas`
    return this.http.post<Plataforma>(url, plataforma).pipe(
      map(obj => obj),
      catchError(e => this.handleError(e))
    )
  }

  read(): Observable<any> {
    const url = `${API_URL}/plataformas`
    return this.http.get<any>(url).pipe(
      map(obj => obj),
      catchError(e => this.handleError(e))
    )
  }

  readById(id: string): Observable<any> {
    const url = `${API_URL}/plataformas/${id}`
    return this.http.get<any>(url).pipe(
      map(obj => obj),
      catchError(e => this.handleError(e))
    )
  }

  update(plataforma: any): Observable<any> {
    const url = `${API_URL}/plataformas/${plataforma.sku}`
    return this.http.put<any>(url, plataforma).pipe(
      map(obj => obj),
      catchError(e => this.handleError(e))
    )
  }

  delete(plataforma: any): Observable<any> {
    const url = `${API_URL}/plataformas/${plataforma.sku}`
    return this.http.delete<any>(url).pipe(
      map(obj => obj),
      catchError(e => this.handleError(e))
    )
  }

  handleError(e: any): Observable<any> {
    console.log('Ocorreu o seguinte erro: ', e);
    this.showMessage('Ocorreu um erro!', true);
    return EMPTY
  }
}
