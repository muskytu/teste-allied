import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../../template/confirm-dialog/confirm-dialog.component';
import { Plataforma } from '../plataforma.model';
import { PlataformaService } from '../plataforma.service';

@Component({
  selector: 'app-plataformas',
  templateUrl: './plataformas.component.html',
  styleUrls: ['./plataformas.component.css']
})
export class PlataformasComponent implements OnInit {

  plataformas: Plataforma[]
  displayedColumns = ['sku', 'nome', 'descricao', 'action']
  result: string = '';

  constructor(
    private dialog: MatDialog,
    private plataformaService: PlataformaService,
  ) {
    this.plataformas = []
  }

  ngOnInit(): void {
    this.carregaDados()
  }

  deletarPlataforma(plataforma: Plataforma) {
    this.plataformaService.delete(plataforma).subscribe(() => {
      this.plataformaService.showMessage('Plataforma deletada com sucesso!')
      this.carregaDados()
    })
  }

  carregaDados() {
    this.plataformaService.read().subscribe(plataformas => {
      console.log('Plataformas', plataformas)
      this.plataformas = plataformas.plataformas
    })
  }

  confirmDialog(plataforma: Plataforma): void {
    const message = `Tem certeza que deseja excluir este dado?`;
    const dialogData = new ConfirmDialogModel("Confirmar", message, plataforma);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.deletarPlataforma(plataforma)
      } else {
        console.log('Valeu a tentativa!!!')
      }
    });
  }

}
