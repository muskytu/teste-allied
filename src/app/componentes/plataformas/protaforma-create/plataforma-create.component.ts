import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Plataforma } from '../plataforma.model';
import { PlataformaService } from '../plataforma.service';

@Component({
  selector: 'app-plataforma-create',
  templateUrl: './plataforma-create.component.html',
  styleUrls: ['./plataforma-create.component.css']
})
export class PlataformaCreateComponent implements OnInit {

  plataforma: Plataforma = {
    sku: '',
    nome: '',
    descricao: ''
  }

  constructor(
    private plataformaService: PlataformaService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id')
    if (id) {
      this.plataformaService.readById(id).subscribe(plataforma => {
        this.plataforma = plataforma
      })
    }
  }

  createPlataforma(): void {
    this.plataformaService.create(this.plataforma).subscribe(() => {
      this.plataformaService.showMessage('Produto criado!')
      this.cancel();
    })
  }

  editPlataforma(): void {
    console.log(this.plataforma)
    this.plataformaService.update(this.plataforma).subscribe(() => {
      this.plataformaService.showMessage('Plataforma atualizada com sucesso!')
      this.cancel();
    })
  }

  cancel(): void {
    this.router.navigate(['plataformas'])
  }

}
