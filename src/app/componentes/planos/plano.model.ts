export interface Plano {
    sku: string
    franquia: string
    valor: string
    ativo: boolean
}