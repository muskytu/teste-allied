import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../../template/confirm-dialog/confirm-dialog.component';
import { FormDialogComponent } from '../../template/form-dialog/form-dialog.component';
import { Plano } from '../plano.model';
import { PlanoService } from '../plano.service';

@Component({
  selector: 'app-planos',
  templateUrl: './planos.component.html',
  styleUrls: ['./planos.component.css']
})
export class PlanosComponent implements OnInit {

  planos: Plano[]
  planoSelecionado: string = '';
  displayedColumns = ['sku', 'franquia', 'valor', 'ativo', 'action']
  result: string = '';

  constructor(
    private dialog: MatDialog,
    private planoService: PlanoService,
    private route: ActivatedRoute
  ) {
    this.planos = []
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id')
    if (id) {
      this.carregaDados(id)
      this.planoSelecionado = id;
    }
  }

  carregaDados(id: string) {
    this.planoService.readById(id).subscribe(planos => {
      console.log('Planos', planos)
      this.planos = planos.planos
    })
  }

  confirmDialog(plano: Plano): void {
    const message = `Plano desativado portando não é possível enviar o formulário?`;
    const dialogData = new ConfirmDialogModel("Aviso", message, plano);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        console.log('Boa tentativa!')
      } else {
        console.log('Não foi desta vez!')
      }
    });
  }

  openFormDialog(plano: Plano): void {
    const dialogRef = this.dialog.open(FormDialogComponent, {
      width: "50vw",
      data: {
        title: 'Plano',
        nome: '',
        email: '',
        telefone: '',
        cpf: '',
        nascimento: '',
        plano: plano,
        planoSelecionado: this.planoSelecionado
      }
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {

        console.log('Cadastro efetuado com sucesso => ', dialogResult)
      } else {
        console.log('Não foi desta vez!')
      }
    });
  }

}
