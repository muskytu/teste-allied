import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EMPTY, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { API_URL } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PlanoService {

    constructor(
        private snackBar: MatSnackBar,
        private http: HttpClient
    ) { }

    showMessage(msg: string, isError: boolean = false): void {
        this.snackBar.open(msg, 'x', {
            duration: 3000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: isError ? ['msg-error'] : ['msg-success'],
        })
        console.log(msg)
    }

    read(): Observable<any> {
        const url = `${API_URL}/planos`
        return this.http.get<any>(url).pipe(
            map(obj => obj),
            catchError(e => this.handleError(e))
        )
    }

    readById(id: string): Observable<any> {
        const url = `${API_URL}/planos/${id}`
        return this.http.get<any>(url).pipe(
            map(obj => obj),
            catchError(e => this.handleError(e))
        )
    }

    delete(plano: any): Observable<any> {
        const url = `${API_URL}/planos/${plano.sku}`
        return this.http.delete<any>(url).pipe(
            map(obj => obj),
            catchError(e => this.handleError(e))
        )
    }

    handleError(e: any): Observable<any> {
        console.log('Ocorreu o seguinte erro: ', e);
        this.showMessage('Ocorreu um erro!', true);
        return EMPTY
    }
}
