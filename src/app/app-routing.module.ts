import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlanosComponent } from './componentes/planos/planos/planos.component';
import { PlataformasComponent } from './componentes/plataformas/plataformas/plataformas.component';
import { HomeComponent } from './views/home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  }, {
    path: 'plataformas',
    component: PlataformasComponent
  }, {
    path: 'planos/:id',
    component: PlanosComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
