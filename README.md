Este projeto foi gerado com [Angular CLI] (https://github.com/angular/angular-cli) versão 12.1.2.

## Servidor de desenvolvimento

Antes de tudo execute `npm install` para rodar todas as importações do projeto

Execute `npm start` ou `ng serve` para um servidor de desenvolvimento. Navegue até `http://localhost:4200/`. O aplicativo será carregado automaticamente

## O Teste

O sistema consiste basicamente em uma página inicial, com um menu que contém acesso às plataformas.
 
Ao clicar em uma plataforma você acessa um plano
 
E ao clicar para ver um plano você acessa um formulário para cadastrar suas informações que ao submeter será exibido os dados no console.
